const graph = {}
graph.a = ['b', 'c']
graph.b = ['f']
graph.c = ['d', 'e']
graph.d = ['f']
graph.e = ['f']
graph.f = ['g']
const breadthSearch = (graph, start, end) => {
    let que = []
    que.push(start)
    while (que.length > 0) {
        const current = que.shift()
        if (!graph[current]) graph[current] = []
        if (graph[current].includes(end)) return true
        else que = [...que, ...graph[current]]
    }
    return false
}
console.log(breadthSearch(graph, 'a', 'e'))